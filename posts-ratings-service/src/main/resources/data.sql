INSERT INTO POST (id, user_id, created, modified, body) VALUES 
(1,	'DoctorOctopus', 	'2017-05-04 10:22:00', '2017-05-04 10:22:00', 'Wipe out Spiderman by creating a stronger spider-based being'), 
(2,	'Hobgoblin', 		'2017-05-04 17:12:00', '2017-05-04 17:12:00', 'Destroy Spiderman by trapping him underneath a large glass'),
(3,	'Green Goblin', 	'2017-05-04 18:59:00', '2017-05-04 18:59:00', 'Crush Spiderman in an industrial pressure chamber'),
(4,	'DoctorOctopus', 	'2017-05-05 08:03:00', '2017-05-05 08:03:00', 'Annihilate Spiderman using bug spray'),
(5,	'DoctorOctopus', 	'2017-05-05 11:17:00', '2017-05-05 11:17:00', 'End Spiderman by turning the city against him'),
(6,	'Hobgoblin', 		'2017-05-06 10:56:00', '2017-05-06 10:56:00', 'Eradicate Spiderman in a car crusher'),
(7,	'Green Goblin', 	'2017-05-06 12:09:00', '2017-05-06 12:09:00', 'Butcher Spiderman in front of a crowd'),
(8,	'DoctorOctopus', 	'2017-05-06 16:45:00', '2017-05-06 16:45:00', 'Liquify Spiderman with spider venom');

INSERT INTO RATING (id, rating, user_id) VALUES
(1, 'POSITIVE', 'DoctorOctopus'),
(2, 'POSITIVE', 'Hobgoblin'),
(3, 'POSITIVE', 'DoctorOctopus'),
(4, 'NEGATIVE', 'DoctorOctopus'),
(5, 'POSITIVE', 'DoctorOctopus'),
(6, 'NEGATIVE', 'Hobgoblin'),
(7, 'POSITIVE', 'DoctorOctopus'),
(8, 'POSITIVE', 'DoctorOctopus'),
(9, 'NEGATIVE', 'Green Goblin');

INSERT INTO POST_RATINGS (post_id, ratings_id) VALUES
(1, 1),
(1, 2),
(2, 3),
(3, 4),
(4, 5),
(4, 6),
(6, 7),
(7, 8),
(7, 9);