package uk.pcstonham.supervillain.converters;

import org.springframework.core.convert.converter.Converter;

import uk.pcstonham.supervillain.entities.Post;
import uk.pcstonham.supervillain.model.PostModel;

public class PostModelToPostConverter implements Converter<PostModel, Post> {

	public Post convert(PostModel postModel) {
		final Post post = new Post();
		post.setBody(postModel.getBody());
		post.setUserId(postModel.getUserId());
		return post;
	}
}
