package uk.pcstonham.supervillain.repositories;

import java.time.LocalDateTime;

import org.springframework.data.repository.PagingAndSortingRepository;

import uk.pcstonham.supervillain.entities.Post;

public interface PostRepository extends PagingAndSortingRepository<Post, Long> {

	Iterable<Post> findByUserId(String userId);
	
	Iterable<Post> findByCreatedBetween(LocalDateTime from, LocalDateTime to);
	
	Iterable<Post> findByBodyContaining(String searchCriteria);
}
