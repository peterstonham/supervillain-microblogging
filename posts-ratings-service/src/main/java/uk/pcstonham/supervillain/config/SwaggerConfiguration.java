package uk.pcstonham.supervillain.config;

import org.springframework.context.annotation.Configuration;

import io.swagger.annotations.Api;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfiguration {

	public Docket springConfigurationDocket() {
		return new Docket(DocumentationType.SWAGGER_2)
				.apiInfo(new ApiInfoBuilder().contact(
						new Contact("Peter Stonham", "", "peter@stonhams.net"))
						.title("Supervillain Microblogging Application").build())
				.select()
				.apis(RequestHandlerSelectors.withClassAnnotation(Api.class))
				.build();
	}
}