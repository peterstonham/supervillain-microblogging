package uk.pcstonham.supervillain.config;

import java.util.HashSet;
import java.util.Set;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ConversionServiceFactoryBean;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;

import uk.pcstonham.supervillain.converters.PostModelToPostConverter;

@Configuration
public class ConversionConfiguration {
	
	@Bean(name = "explicitConversionService")
	public ConversionService conversionService() {
		ConversionServiceFactoryBean bean = new ConversionServiceFactoryBean();
		bean.setConverters(getAllConverters());
		bean.afterPropertiesSet();
		return bean.getObject();
	}
	
	private Set<Converter<?,?>> getAllConverters() {
		final Set<Converter<?,?>> converters = new HashSet<Converter<?,?>>();
		
		converters.add(new PostModelToPostConverter());
		
		return converters;
	}
}
