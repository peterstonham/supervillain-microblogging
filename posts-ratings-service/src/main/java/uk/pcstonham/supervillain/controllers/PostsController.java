package uk.pcstonham.supervillain.controllers;

import java.security.Principal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.convert.ConversionService;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import uk.pcstonham.supervillain.entities.Post;
import uk.pcstonham.supervillain.entities.Rating;
import uk.pcstonham.supervillain.entities.RatingValue;
import uk.pcstonham.supervillain.model.PostModel;
import uk.pcstonham.supervillain.model.ValidationError;
import uk.pcstonham.supervillain.repositories.PostRepository;

@RestController
@Api
public class PostsController {
	
	private final PostRepository postRepository;
	private final ConversionService conversionService;
	
	@Autowired
	public PostsController(PostRepository postRepository, @Qualifier("explicitConversionService") ConversionService conversionService) {
		this.postRepository = postRepository;
		this.conversionService = conversionService;
	}

	@ApiOperation("Create a new post for the logged-in user")
	@PostMapping(path = "/posts")
	@ResponseStatus(HttpStatus.CREATED)
	public @ResponseBody Post createNewPost(@RequestBody @Valid final PostModel postModel, Principal principal) {
		if (principal != null) {
			postModel.setUserId(principal.getName());
		}
		final Post post = conversionService.convert(postModel, Post.class);
		return postRepository.save(post);
	}
	
	@ApiOperation("Delete a post")
	@DeleteMapping(path = "/posts/{postId}")
	public ResponseEntity<?> deletePost(@RequestParam("postId") final Long postId, Principal principal) {
		final Post post = postRepository.findOne(postId);
		if (post == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		if (principal != null) {
			if (principal.getName().equals(post.getUserId())) {
				throw new UnauthorisedException();
			}
		}
		postRepository.delete(postId);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
	@ApiOperation("Update a post")
	@PutMapping(path = "/posts/{postId}")
	public ResponseEntity<?> updatePost(@RequestParam("postId") final Long postId, Principal principal) {
		final Post post = postRepository.findOne(postId);
		if (post == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		if (principal != null) {
			if (principal.getName().equals(post.getUserId())) {
				throw new UnauthorisedException();
			}
		}
		return new ResponseEntity<>(postRepository.save(post), HttpStatus.ACCEPTED);
	}
	
	@ApiOperation(value = "List all posts")
	@GetMapping(path = "/posts")
	public @ResponseBody Iterable<Post> retrieveAllPosts() {
		return postRepository.findAll();
	}
	
	@ApiOperation("List all posts for the provided user id")
	@GetMapping(path = "/postsByUser")
	public @ResponseBody Iterable<Post> retrieveAllPostsForUserId(@RequestParam("userId") final String userId) {
		return postRepository.findByUserId(userId);
	}
	
	@ApiOperation("List all posts for the provided date range")
	@GetMapping(path = "/postsByDateRange")
	public @ResponseBody Iterable<Post> retrieveAllPostsInDateRange(
			@RequestParam("dateFrom") 
			@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") 
			@ApiParam("The date_time at the beginning of the search range, in the format 'yyyy-MM-dd HH:mm:ss'")
			final LocalDateTime dateFrom, 
			@RequestParam("dateTo")
			@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
			@ApiParam("The date_time at the beginning of the search range, in the format 'yyyy-MM-dd HH:mm:ss'")
			final LocalDateTime dateTo,
			@RequestParam("orderAscending")
			@ApiParam("True if the results should be returned ascending, false if the results should be descending")
			final boolean orderAscending) {
		
		final List<Post> posts = new ArrayList<>(); 
		postRepository.findByCreatedBetween(dateFrom, dateTo).forEach(p -> posts.add(p));
		final Comparator<Post> comparator = Comparator.comparing(Post::getCreated);
		if (orderAscending) {
			Collections.sort(posts, comparator.reversed());
		} else {
			Collections.sort(posts, comparator);
		}
		return posts;
	}
	
	@ApiOperation("List all posts containing the specified text string")
	@GetMapping(path = "/postsMatchingTextString")
	public @ResponseBody Iterable<Post> retrieveAllPostsMatchingString(
			@RequestParam("queryField") 
			@ApiParam("The text string to search for within the body of the post")
			final String queryField) {
		
		return postRepository.findByBodyContaining(queryField);
	}
	
	@ApiOperation("Rate a specified post")
	@PostMapping(path = "/posts/{postId}/rate/{ratingType}")
	@ResponseStatus(HttpStatus.CREATED)
	public @ResponseBody Post ratePostById(@PathVariable("postId") final Long postId,
			@PathVariable("ratingType") final RatingValue ratingType, Principal principal) {
		final Post post = postRepository.findOne(postId);
		if (principal != null) {
			post.getRatings().forEach(r -> {
				if (r.getUserId().equals(principal.getName()))
					throw new AlreadyRatedThisPostException();
			});
		}
		final Rating rating = new Rating();
		rating.setRating(ratingType);
		if (principal != null) {
			rating.setUserId(principal.getName());
		}
		post.getRatings().add(rating);
		return postRepository.save(post);
	}
	
	@ExceptionHandler
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ValidationError handleException(MethodArgumentNotValidException exception) {
		return ValidationError.buildFromBindingErrors(exception.getBindingResult());
	}
	
	@ExceptionHandler
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ValidationError handleException(AlreadyRatedThisPostException exception) {
		return new ValidationError(exception.getMessage());
	}
	
	@ExceptionHandler
	@ResponseStatus(HttpStatus.FORBIDDEN)
	public ValidationError handleException(UnauthorisedException exception) {
		return new ValidationError(exception.getMessage());
	}
	
	class UnauthorisedException extends RuntimeException {
		public UnauthorisedException() {
			super("You are not allowed to do that");
		}
	}
	
	class AlreadyRatedThisPostException extends RuntimeException {
		public AlreadyRatedThisPostException() {
			super("This post has already been rated by this user.");
		}
	}
}
