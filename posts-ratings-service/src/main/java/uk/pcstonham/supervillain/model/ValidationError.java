package uk.pcstonham.supervillain.model;

import java.util.ArrayList;
import java.util.List;

import org.springframework.validation.Errors;

import com.fasterxml.jackson.annotation.JsonInclude;

public class ValidationError {

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private List<String> errors = new ArrayList<>();

	private final String errorMessage;

	public ValidationError(final String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public void addValidationError(final String error) {
		this.errors.add(error);
	}

	public List<String> getErrors() {
		return errors;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public static ValidationError buildFromBindingErrors(final Errors errors) {
		ValidationError error = new ValidationError(String.format("Validation failed: %d error%s",
				errors.getErrorCount(), (errors.getErrorCount() > 1 ? "s" : "")));
		errors.getAllErrors().forEach(oe -> error.addValidationError(oe.getDefaultMessage()));
		return error;
	}
	
}
