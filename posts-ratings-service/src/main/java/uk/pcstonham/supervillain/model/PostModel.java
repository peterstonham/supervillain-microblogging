package uk.pcstonham.supervillain.model;

import org.hibernate.validator.constraints.NotBlank;

public class PostModel {

	// User ID is provided by the principal (i.e. logged-in user) and so has no
	// validation on it
	private String userId;
	// Timestamp is populated on insertion into the database and so isn't
	// provided to the database service.
	// Post ID is auto-generated by hibernate and so isn't provided to the
	// database service.
	@NotBlank(message = "A body for the Post must be provided")
	private String body;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

}
