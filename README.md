# Supervillain Microblogging
A sample application written in Java using Spring Boot/Cloud as the microservice framework

## Running the application
### Manual Running
There is a single microservice providing the RESTful interface for the Posts/Ratings function.
This can be run by building the parent POM file then building the posts-ratings-service POM file.
```shell
mvn install -f ./parent/pom.xml
mvn package -f ./posts-ratings-service/pom.xml
```
Once the Maven projects have been built, the posts-ratings-service can be run.
```shell
java -jar ./posts-ratings-service/target/posts-ratings-service-0.0.1-SNAPSHOT.jar
```
The posts-ratings-service runs on port 11589 by default, and can be accessed by sending a GET request to `http://127.0.0.1:11589/`
Swagger documentation of the API is available at `http://127.0.0.1:11589/swagger-ui.html`.

The port on which the service runs can be overriden by setting the `server.port` property on runtime:
```shell
java -jar ./posts-ratings-service/target/posts-ratings-service-0.0.1-SNAPSHOT.jar -Dserver.port=8080
```

### Docker
The application has been configured such that it can be run in Docker.
To run in Docker, build the parent POM and then build a Docker image.
The following command will build a Docker image and publish it to the local Docker repository.
```shell
mvn install -f ./parent/pom.xml
mvn package docker:build ./posts-ratings-service/pom.xml
```
This can then be run using the Docker run command, ensuring that the port is mapped to an appropriate port.
E.g. to expose the application on the Docker host at port 8080, run the following command:
```shell
docker run -d -p 8080:11589 pcstonham/posts-ratings-service
```

#### Clustering & Containerisation
Containerisation and clustering go hand-in-hand; a single service running in a container can be instantiated multiple times.
Load balancing can be implemented in multiple ways:
  - Commercial load balancer routing requests to each service (exposed over different ports)
  - Registration of the microservices with a Discovery server (e.g. Eureka) and configuration of load balancing within this server.

Other considerations with clustering would be:
  + Database clustering and persistence 
      * Does the database sit in a single location and communicate with multiple microservice?
      * Do the microservice instances post inbound data to a shared message queue, and implement eventual consistency with regards to the reading of data?

## Tasks:
### Must:
- [x] Store Posts and Ratings (where Post stores Timestamp, Body and User ID (optional), and Rating stores the rating value and the user ID (optional).
- [x] Provide access to the Post and Rating services through a RESTful API.
  + CRUD operations which must be provided:
    * [x] Retrieve a single post
    * [x] Retrieve a list of posts
    * [x] Filter and sort posts by user ID and timestamp
    * [x] Create new post
    * [x] Delete existing post
    * [x] Update existing post
    * [x] Rate existing post
    * [x] View ratings for posts
- [x] Use Maven as a build tool for the application
- [x] Provide the source code
- [x] Provide a Dockerfile or Docker image

### Should:
- [x] Include Swagger documentation of the interface
- [ ] Include User Management / Security
- [x] Allow for text searching of the posts' contents

### Could:
I've not yet implemented any of these features due to time constraints and so that a solution can be delivered.
- [ ] Add a Config server for the different microservices
- [ ] Add a simple UI for the REST interface
- [ ] Add a discovery server for load balancing/clustering
- [ ] Implement a docker-compose based deployment with a clustering example